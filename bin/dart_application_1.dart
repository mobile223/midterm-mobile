import 'package:dart_application_1/dart_application_1.dart'
    as dart_application_1;

import 'dart:io';

List tokenizing(String ex) {
  var t1 = ex.split('');

  var t2 = new List<String>.filled(0, "m", growable: true);
  for (var token in t1) {
    if (token != " ") {
      t2.add(token);
    }
  }

  return t2;
}

bool isInteger(String str) {
  for (var i = 0; i <= 9; i++) {
    try {
      if (int.parse(str) == i) {
        return true;
      }
    } on Exception catch (e) {}
  }
  return false;
}
bool isOperator(String str) {
  for (String t in {"+", "-", "*", "/", "%", "^"}) {
    if (str.compareTo(t) == 0) {
      return true;
    }
  }
  return false;
}

int precedences(String str) {
  switch (str) {
    case "+":
    case "-":
      return 1;
    case "*":
    case "/":
    case "%":
      return 2;
    case "^":
      return 3;
    default:
      return 0;
  }
}


List infixToPostfix(List infix) {
  var oprList = new List<String>.filled(0, "m", growable: true);
  var postfix = new List<String>.filled(0, "m", growable: true);

  for (var token in infix) {
    if (isInteger(token)) {
      postfix.add(token);
    }
    if (isOperator(token)) {
      while (oprList.isNotEmpty &&
          (oprList.last.compareTo("(") != 0) &&
          (precedences(token) <= precedences(oprList.last))) {
        postfix.add(oprList.removeLast());
      }
      oprList.add(token);
    }

  }
  return postfix;
}

void main() {
  String ex = stdin.readLineSync()!;
  var tokens = tokenizing(ex);
  print("Tokens : $tokens");
  var postfix = infixToPostfix(tokens);
  print(" postfix =========>: \n$postfix");
}
